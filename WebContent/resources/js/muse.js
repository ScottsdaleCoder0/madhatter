$('document').ready(function(){
	//Handle the deletion of a hat.
	$('.delete-hat-from-list').click(function(e){
		e.preventDefault()
		if(window.confirm('Are you sure you want to delete that hat?')){
			console.log('/madhatter/product/delete/' + e.target.id)
			$.ajax({
				url: '/madhatter/product/delete/' + e.target.id,
				method: 'DELETE',
				async: false,
				//success: setTimeout(window.location.reload(), 40),
				error: function(xhr, status, error) {
				  var err = eval("(" + xhr.responseText + ")");
				  alert(err.Message);
				}
			})
		}
	})
})