<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    	<li class="nav-item"><a class="nav-link" href="/madhatter/product/home">Home</a></li>
		<li class="nav-item"><a class="nav-link" href="/madhatter/product/list">List Product</a></li>
		<li class="nav-item"><a class="nav-link" href="/madhatter/product/createProduct">Create product</a></li>
		<li class="nav-item"><a class="nav-link" href="/madhatter/user/list">List Users</a></li>
		<li class="nav-item"><a class="nav-link" href="/madhatter/user/logout">Log out</a></li>	
    </ul>
  </div>
</nav>