<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <c:if test="${username == null}">
			<li class="nav-item"><a class="nav-link" href="/madhatter/login">Login</a></li>
			<li class="nav-item"><a class="nav-link" href="/madhatter/register">Register</a></li>
		</c:if>
		
		<!-- User is logged in, conditional admin view -->
		<c:if test="${username != null}">
			<li class="nav-item"><a class="nav-link" href="/madhatter/product/home">Home</a></li>
			<c:if test="${isAdmin}">
				<li class="nav-item"><a class="nav-link" href="/madhatter/product/list">Admin</a></li>
			</c:if>
			<li class="nav-item"><a class="nav-link" href="/madhatter/user/cart/${username}">Cart</a></li>
			<li class="nav-item"><a class="nav-link" href="/madhatter/user/logout">Log out</a></li>
		</c:if>		
    </ul>
  </div>
</nav>