<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<div class="d-flex container justify-content-center h-100">
			<div class="h-auto align-self-center bg-secondary p-4">
				<div class="content-title">Register</div>
				<form:form method="POST" action="${pageContext.request.contextPath}/user/register" modelAttribute="user" class="d-grid">
					<div>
						<div class="form-group">
							<form:label path="fullName" class="">Full name</form:label>
							<form:input path="fullName" class="" />				
						</div>
					</div>
					<div>
						<form:errors path="fullName" cssClass="form-error" element="div" />
					</div>
					<div>
						<div class="form-group">
							<form:label path="username" class="">UserName</form:label>
							<form:input path="username" class="" />				
						</div>
					</div>
					<div>
						<form:errors path="username" cssClass="form-error" element="div" />
					</div>
					<div>
						<div class="form-group">
							<form:label path="email" class="">Email</form:label>
							<form:input path="email" class="" />				
						</div>
					</div>
					<div>
						<form:errors path="email" cssClass="form-error" element="div" />
					</div>
					<div>
						<div class="form-group">
							<form:label path="password" class="">Password</form:label>
							<form:input type="password" path="password" class="" />
						</div>
					</div>
					<div>
						<form:errors path="password" cssClass="form-error" element="div" />					
					</div>		
					<div>
						<div class="form-group">
							<form:label path="confPassword" class="">Confirm Password</form:label>
							<form:input type="password" path="confPassword" class="" />
						</div>
					</div>
					<div>
						<form:errors path="confPassword" cssClass="form-error" element="div" />					
					</div>					
					<div>
						<input type="submit" value="Register" class="" />
					</div>
				</form:form>
			</div>
		</div>
	</body>
</html>