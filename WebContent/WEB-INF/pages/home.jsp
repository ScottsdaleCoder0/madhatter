<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<div class="d-grid container bg-secondary h-auto">
			<div class="row">			
				<c:forEach items="${payload}" var="hat">
					<div class="col-12 col-md-3 m-2">
						<div class="card">
						<img class="card-img-top" src="<c:url value="/resources/images/noImage.png" />" />
						<div class="card-body">
							<h5 class="card-title">${hat.type}</h5>
							<a class="btn btn-dark" href="/madhatter/product/item/${hat.id}">Purchase</a>
						</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</body>
</html>