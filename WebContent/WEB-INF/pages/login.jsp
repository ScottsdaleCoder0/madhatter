<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<div class="d-flex container justify-content-center h-100">
			<div class="h-auto align-self-center bg-secondary p-4">
				<div class="content-title">Login</div>
					<form:form method="POST" action="${pageContext.request.contextPath}/user/login" modelAttribute="user" class="d-grid">
						<div class="form-group">
							<form:label path="email" class="">Email</form:label>
							<form:input path="email" class="" />				
						</div>
						<div class="form-group">
							<form:errors path="email" cssClass="form-error" element="div" />
						</div>
						<div class="form-group">
							<form:label path="password" class="">Password</form:label>
							<form:input type="password" path="password" class="" />
						</div>
						<div class="form-group">
							<form:errors path="password" cssClass="form-error" element="div" />					
						</div>					
						<div class="form-group">
							<input type="submit" value="Log in" class="" />
						</div>
					</form:form>
				</div>
			</div>
	</body>
</html>