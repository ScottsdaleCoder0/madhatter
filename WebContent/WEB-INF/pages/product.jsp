<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<div class="container bg-secondary my-4">
			<div class="d-grid">
				<div class="row">
					<div class="col-6 p-4">
						<img class="img-fluid w-100 h-100" src="<c:url value="/resources/images/noImage.png" />" />
					</div>
					<div class="col-6 p-4">
						<div class="card" style="width: 18rem;">
						  <ul class="list-group list-group-flush">
						  	<li class="list-group-item">Type: ${payload.type}</li>
						    <li class="list-group-item">Size: ${payload.size}</li>
						    <li class="list-group-item">Color: ${payload.color}</li>
						    <li class="list-group-item">Quantity: ${payload.quantity}</li>
						    <li class="list-group-item">Price: ${payload.price}</li>
						  </ul>
						</div>
						<form:form method="POST" action="${pageContext.request.contextPath}/product/purchase" modelAttribute="product" class="d-grid">
							<form:input type="hidden" path="id" class="" />
							<form:input type="hidden" path="type" class="" />				
							<form:input type="hidden" path="color" class="" />
							<form:input type="hidden" path="size" class="" />
							<form:input type="hidden" path="quantity" class="" />
							<form:input type="hidden" path="price" class="" />
							<input type="submit" value="Submit" class="btn btn-dark mt-3" />
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>