<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<div class="container bg-secondary mt-4" style="overflow-x: auto;">
			<table id="products" class="display">
				<thead>
					<tr>
						<td>Type</td>
						<td>Size</td>
						<td>Quantity</td>
						<td>Price</td>
						<td>Actions</td>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		
		<script type="text/javascript">
			var dataTableData
			var dataTable
			function displayTable(){
				dataTable = $("#products").DataTable({
					data: dataTableData,
					columns: [{data: "type"},{data: "size"},{data:"quantity"},{data: "price"},
						{
							data: null,
							render: function(data, type, row){
								return '<div class="d-inline-block"><a href="/madhatter/product/edit/'+data.id+'"><button>Edit</button></a><button id="'+data.id+'" onclick="deleteProduct(event)">Delete</button></div>'
							}
						}],
					createdRow: function (row, data, dataIndex) {
				        $(row).attr('id', 'row-' + data.id);
				    }
				})
				
				
			}
			
			function getProducts(){
				$.ajax({
					type: "GET",
					url: "/madhatter/product/rest/list",
					success: function(data){
						dataTableData = data
						displayTable()
					},
					error: function(xhr, ajaxOptions, thrownError){
						alert(xhr.status);
						alert(thrownError)
					}
				})
			}
			
			function deleteRow(id){
				dataTable.data().row($("#row-"+id)).remove().draw()
			}
			
			function deleteProduct(e){
				var id = e.target.id
				if(window.confirm('Are you sure you would like to delete this product?' + id)){
					$.ajax({
						type: "DELETE",
						url: "/madhatter/product/rest/delete/"+id,
						success: deleteRow(id),
						error: function(xhr, ajaxOptions, thrownError){
							console.log(xhr.status);
							console.log(thrownError)
							alert('We\'re sorry, something went wrong')
						}
					})
				}	
			}
			
			$(document).ready(getProducts)
		</script>
	</body>
</html>