<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Mad Hatter: Create Product</title>
	</head>
	<body>
		<div class="d-flex container justify-content-center h-100">
			<div class="h-50 align-self-center bg-secondary p-4">
				<div class="content-title">Edit Product</div>
				<form:form method="POST" action="${pageContext.request.contextPath}/product/edit" modelAttribute="payload" class="d-grid">
					<form:input type="hidden" path="id"/>
					<div>
						<div class="form-group">
							<form:label path="type" class="">Type</form:label>
							<form:input path="type" class="" />				
						</div>
					</div>
					<div>
						<form:errors path="type" cssClass="form-error" element="div" />
					</div>
					<div>
						<div class="form-group">
							<form:label path="color" class="">Color</form:label>
							<form:input path="color" class="" />
						</div>
					</div>
					<div>
						<form:errors path="color" cssClass="form-error" element="div" />					
					</div>
					<div>
						<div class="form-group">
							<form:label path="size" class="">Size</form:label>
							<form:input path="size" class="" />
						</div>
					</div>
					<div>
						<form:errors path="size" cssClass="form-error" element="div" />					
					</div>					
					<div>
						<form:errors path="image" cssClass="form-error" element="div" />					
					</div>
					<div>
						<div class="form-group">
							<form:label path="quantity" class="">Quantity</form:label>
							<form:input path="quantity" class="" />
						</div>
					</div>
					<div>
						<form:errors path="quantity" cssClass="form-error" element="div" />					
					</div>
					<div>
						<div class="form-group">
							<form:label path="price" class="">Price</form:label>
							<form:input path="price" class="" />
						</div>
					</div>
					<div>
						<form:errors path="price" cssClass="form-error" element="div" />					
					</div>
					<div>
						<div class="form-group">
							<form:label path="image" class="">Image</form:label>
							<form:input type="file" path="image" class="" />
						</div>
					</div>
					<div>
						<input type="submit" value="Submit" class="" />
					</div>
				</form:form>
			</div>
		</div>
	</body>
</html>