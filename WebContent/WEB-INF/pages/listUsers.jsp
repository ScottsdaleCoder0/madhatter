<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<div class="container bg-secondary mt-4" style="overflow-x: auto;">
			<table id="users" class="display">
				<thead>
					<tr>
						<td>Full name</td>
						<td>User name</td>
						<td>Email</td>
						<td>Admin</td>
						<td>Actions</td>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		
		<script type="text/javascript">
			var dataTableData
			var dataTable
			function displayTable(){
				dataTable = $("#users").DataTable({
					data: dataTableData,
					columns: [{data: "fullName"},{data: "username"},{data:"email"},
						{
							data: null,
							render: function(data, type, row){
								return data.isAdmin ? 'Yes' : 'No'
							}
						},
						{
							data: null,
							render: function(data, type, row){
								return data.isAdmin ? '<div class="d-inline-block"><button id="'+data.username + '-'+data.id+'" onclick="toggleAdmin(event)">Remove Admin</button><button id="'+data.username + '-'+data.id+'" onclick="deleteUser(event)">Delete</button></div>' : '<div class="d-inline-block"><button id="'+data.username + '-'+data.id+'" onclick="toggleAdmin(event)">Make Admin</button><button id="'+data.username + '-'+data.id+'" onclick="deleteUser(event)">Delete</button></div>'
										
							}
						}],
					createdRow: function (row, data, dataIndex) {
				        $(row).attr('id', 'row-' + data.id);
				    }
				})
				
				
			}
			
			function getUsers(){
				$.ajax({
					type: "GET",
					url: "/madhatter/user/rest/list",
					success: function(data){
						console.log(data)
						dataTableData = data
						displayTable()
					},
					error: function(xhr, ajaxOptions, thrownError){
						alert(xhr.status);
						alert(thrownError)
					}
				})
			}
			
			function toggleAdmin(e){
				var username = e.target.id.split('-')[0]
				var id = e.target.id.split('-')[1]
				if(confirm('Are you sure you want to toggle ' + username + '\'s admin status?')){
					$.ajax({
						type: "PUT",
						url: "/madhatter/user/rest/admin/" + id,
						success: function(){
							updateAdminStatus(id)
						},
						error: function(xhr, ajaxOptions, thrownError){
							console.log(xhr.status);
							console.log(thrownError)
							alert('We\'re sorry, something went wrong')
						}
					})
				}
				
			}
			
			function deleteRow(id){
				dataTable.data().row($("#row-"+id)).remove().draw()
			}
			function updateAdminStatus(id){
				var status = dataTable.row($("#row-"+id)).data()['isAdmin']

				var rowData = dataTable.row($("#row-"+id)).data()
				rowData['isAdmin'] = !status
				
				dataTable.row($("#row-"+id)).data(rowData).draw()
			}
			
			function deleteUser(e){
				var username = e.target.id.split('-')[0]
				var id = e.target.id.split('-')[1]
				
				if(window.confirm('Are you sure you want to delete user ' + username + '?')){
					$.ajax({
						type: "DELETE",
						url: "/madhatter/user/rest/delete/"+id,
						success: function(){
							deleteRow(id)
						},
						error: function(xhr, ajaxOptions, thrownError){
							console.log(xhr.status);
							console.log(thrownError)
							alert('We\'re sorry, something went wrong')
						}
					})
				}	
			}
			
			$(document).ready(getUsers)
		</script>
	</body>
</html>