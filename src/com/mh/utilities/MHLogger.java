package com.mh.utilities;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.glassfish.hk2.api.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * A facade class to use application wide for logging statements.
 * </p>
 * @author Nathan Ford
 *
 */
public class MHLogger {
	
	protected static Logger getLogger() {
		return LoggerFactory.getLogger(MHLogger.class);
	}
	
	public static void traceEntry(String className, String methodName) {
		MHLogger.getLogger().trace("Entering " + className + "@" + methodName);
	}
	
	public static void traceExit(String className, String methodName) {
		MHLogger.getLogger().trace("Exiting " + className + "@" + methodName);
	}
	
	public static void errorLog(String className, String methodName, Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		
		MHLogger.getLogger().error(className + "@" + methodName + ":  " + sw.toString());
	}
	
	public static void debugLog(String message) {
		MHLogger.getLogger().debug(message);
	}
	
	public static void infoLog(String message) {
		MHLogger.getLogger().info(message);
	}
	
	public static void warnLog(String message) {
		MHLogger.getLogger().warn(message);
	}
}
