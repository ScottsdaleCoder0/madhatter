package com.mh.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

	/**
	 * <p>
	 * Gets a connection to the database for the dao layer to use
	 * </p>
	 * 
	 * @return The connection to the database
	 * 
	 * @throws SQLException Connection issues occured, handle exception in dao layer.
	 * @throws ClassNotFoundException 
	 */
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		MHLogger.traceEntry("DBConnection", "getConnection");
		
		//Set the parameters
		//NOTE: These must be method level defined to keep this method static
		String url = "jdbc:postgresql://localhost/madhatter";
		String user = "spring";
		String password = "s6eX@a4@Cx";
		
		//Load the driver
		Class.forName("org.postgresql.Driver");
		
		//Set the properties
		MHLogger.infoLog("Setting the properties for the connection");
		Properties props = new Properties();
		props.setProperty("user", user);
		props.setProperty("password", password);
		
		//Return the connection
		MHLogger.infoLog("Getting the connection and returning it");
		MHLogger.traceExit("DBConnection", "getConnection");
		return DriverManager.getConnection(url, props);
	}
}
