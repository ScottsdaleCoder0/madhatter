package com.mh.utilities;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * <p>
 * Facade class to access a user's session
 * </p>
 * @author Nathan Ford
 *
 */
public class SessionUser {
	public static Map<String, Object> getSessionMap() {
		RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
	    ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
	    HttpServletRequest request = attributes.getRequest();
	    HttpSession httpSession = request.getSession(false);
	    
	    Enumeration<String> sessionObjects = httpSession.getAttributeNames();
	    
	    Map<String, Object> sessionMap = new HashMap<String, Object>();
	    
	    while(sessionObjects.hasMoreElements()) {
	    	String next = sessionObjects.nextElement();
	    	MHLogger.debugLog(next);
	    	sessionMap.put(next, httpSession.getAttribute(next));
	    }
	    
	    return sessionMap;
	    
	}
}
