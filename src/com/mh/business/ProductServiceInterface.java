package com.mh.business;

import java.util.List;

/**
 * <p>
 * Interface for product business logic. Any and all logic associated with the
 * product should take place in this interface and child classes.
 * </p>
 * 
 * @author Jordan Nicols
 * 
 * @param <T> The model that is responsible for a product
 *
 */
public interface ProductServiceInterface<T> {
	
	/**
	 * <p>
	 * Will take a product model and send to data layer to be stored in database
	 * </p>
	 * 
	 * @param product that will be stored
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void createProduct(T product) throws Exception;
	
	/**
	 * <p>
	 * Takes a product and updates it in the database.
	 * </p>
	 * 
	 * @return The new product information for the presentation layer
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void updateProduct(T product) throws Exception;
	
	/**
	 * <p>
	 * Calls the dao to delete the account based off it's primary key.
	 * </p>
	 * @param id The id of the account to be deleted
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void deleteProductById(int id) throws Exception;
	
	/**
	 * <p>
	 * Calls the dao to get a list of all the products in the database
	 * </p>
	 * 
	 * @return A list containing all the products in the database
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public List<T> listProducts() throws Exception;
	
	/**
	 * <p>
	 * Gets an instance of a product from it's database id
	 * </p>
	 * @return The product retrieved
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public T getProductById(int id) throws Exception;
	
	/**
	 * <p>
	 * Decreases the count of the product by one and updates the product in the database.
	 * <b>Ensure you are not decreasing the count before passing the product to this method</b>
	 * </p>
	 * @param product The full product to be purchased
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void purchaseProduct(T product) throws Exception;
}
