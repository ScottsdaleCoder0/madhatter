package com.mh.business;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mh.Product;
import com.mh.dao.DataAccessInterface;
import com.mh.exception.InternalServerException;
import com.mh.utilities.MHLogger;


/**
 * <p>
 * Implementation class of the ProductServiceInterface to be used for interaction with account
 * operations.
 * </p>
 * 
 * @author Jordan Nicols and Nathan Ford
 *
 */

public class ProductServiceImpl implements ProductServiceInterface<Product> {
	
	DataAccessInterface<Product, String> productdao;
	public void setProductdao(DataAccessInterface<Product, String> productdao) {
		this.productdao = productdao;
	}
	
	
	@Override
	public void createProduct(Product product) throws Exception {
		MHLogger.traceEntry("ProductServiceImpl", "createProduct");
		
		// Send the product information to the dao
		MHLogger.infoLog("Sending product model to be added by the dao");
		this.productdao.createT(product);
		
		MHLogger.traceExit("ProductServiceImpl", "createProduct");
	}

	@Override
	public void updateProduct(Product product) throws Exception {
		MHLogger.traceEntry("ProductServiceImpl", "updateProduct");
		MHLogger.debugLog(product.getId() + "");
		this.productdao.updateT(product);
		MHLogger.traceExit("ProductServiceImpl", "updateProduct");
		
	}

	@Override
	public void deleteProductById(int id) throws Exception {
		this.productdao.deleteTById(id);
	}

	@Override
	public List<Product> listProducts() throws Exception {
		MHLogger.traceEntry("ProductServiceImpl", "listProducts");

		//Get the user's full account from the database
		MHLogger.infoLog("Getting the products from the dao");
		ResultSet daors = this.productdao.readListT();
		
		List<Product> products = new ArrayList<Product>();
		//Get the next/first row of the return
		try {
			while(daors.next()) {
				products.add(new Product(
						daors.getInt("hats_id"),
						daors.getString("hat_type"), 
						daors.getString("color"), 
						daors.getString("hat_size"), 
						daors.getBytes("image"), 
						daors.getInt("quantity"), 
						daors.getDouble("price")));
			}	
			
			MHLogger.traceExit("ProductServiceImpl", "listProducts");
			return products;
		} catch(Exception e) {
			//Print error and let front end know what went wrong
			MHLogger.errorLog("ProductServiceImpl", "listProducts", e);
			MHLogger.traceExit("ProductServiceImpl", "listProducts");
			throw new InternalServerException("There was an issue when retrieving the products.");
		} finally {
			//Close the result set
			daors.close();
		}
	}

	@Override
	public Product getProductById(int id) throws Exception {
		MHLogger.traceEntry("ProductServiceImpl", "getProductById");
		
		MHLogger.infoLog("Getting the product rs from the dao layer.");
		ResultSet daors = this.productdao.readTById(id);
		try {
			MHLogger.infoLog("Generating the product object");
			daors.next();
			Product prod = new Product(
					daors.getInt("hats_id"),
					daors.getString("hat_type"), 
					daors.getString("color"), 
					daors.getString("hat_size"), 
					daors.getBytes("image"), 
					daors.getInt("quantity"), 
					daors.getDouble("price"));
			
			MHLogger.traceExit("ProductServiceImpl", "getProductById");
			return prod;
		}catch(Exception e) {
			MHLogger.errorLog("ProductServiceImpl", "getProductById", e);
			MHLogger.traceExit("ProductServiceImpl", "getProductById");
			throw new InternalServerException("There was an issue retrieving the product.");
		}finally {
			MHLogger.infoLog("Closing the result set");
			daors.close();
		}
	}


	@Override
	public void purchaseProduct(Product product) throws Exception {
		MHLogger.traceEntry("ProductServiceImpl", "purchaseProduct");
		//Reduce the count of the product
		product.setQuantity(product.getQuantity() - 1);
		
		//Call to update the product in the database
		this.productdao.updateT(product);
		
		MHLogger.traceExit("ProductServiceImpl", "purchaseProduct");
	}
}
