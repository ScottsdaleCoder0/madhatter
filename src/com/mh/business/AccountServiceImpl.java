/**
 * 
 */
package com.mh.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import com.mh.User;
import com.mh.dao.DataAccessInterface;
import com.mh.exception.InternalServerException;
import com.mh.exception.UserNotAuthenticatedException;
import com.mh.utilities.MHLogger;


/**
 * <p>
 * Implementation class of the AccountServiceInterface to be used for interaction with account
 * operations.
 * </p>
 * 
 * @author Nathan Ford
 *
 */
public class AccountServiceImpl implements AccountServiceInterface<User> {

	DataAccessInterface<User, String> userdao;
	
	public void setUserdao(DataAccessInterface<User, String> userdao) {
		this.userdao = userdao;
	}
	
	@Override
	public void createUserAccount(User user) throws Exception {
		MHLogger.traceEntry("AccountServiceImpl", "createUserAccount");
		//Generate a salt for the user
		MHLogger.infoLog("Calling to get salt");
		String salt = this.generateSalt();
		
		//Hash the users password
		try {
			MHLogger.infoLog("Calling to hash password");
			user.setPassword(this.hashPassword(user.getPassword(), salt));			
		}catch(Exception e) {
			MHLogger.errorLog("AccountServiceImpl", "createUserAccount", e);
		}
		
		
		//Send the account information with the salt to the dao
		MHLogger.infoLog("Sending model to be added by the dao");
		this.userdao.createT(user, salt);
		
		MHLogger.traceExit("AccountServiceImpl", "createUserAccount");
	}

	@Override
	public User authenticateUser(User user) throws Exception {
		MHLogger.traceEntry("AccountServiceImpl", "authenticateUser");

		//Get the user's full account from the database
		MHLogger.infoLog("Getting the user's information from the dao layer");
		ResultSet daors = userdao.readTByUniqueField(user.getEmail());
		String salt;
		User daouser;
		//Get the next/first row of the return
		try {
			
			daors.next();
			//Generate the user model
			daouser = new User(
					daors.getInt("profile_id"),
					daors.getString("full_name"), 
					daors.getString("email"), 
					daors.getString("user_name"), 
					daors.getString("password"),
					daors.getBoolean("admin")
					);
			//Capture the salt
			salt = daors.getString("salt");
			
			
		} catch(Exception e) {
			//Print error and let front end know what went wrong
			MHLogger.errorLog("AccountServiceImpl", "authenticateUser", e);
			throw new InternalServerException("There was an issue when retrieving the account.");
		} finally {
			//Close the result set
			daors.close();
		}
		
		//Hash the password of the attempted login model
		MHLogger.infoLog("Hashing password for attempted login");
		try {
			user.setPassword(this.hashPassword(user.getPassword(), salt));
		}catch(Exception e) {
			MHLogger.errorLog("AccountServiceImpl", "authenticateUser", e);
			throw new InternalServerException("There was in issue verifying the account");
		}
		
		//Compare the passwords
		MHLogger.infoLog("Comparing passwords");
		if(daouser.getPassword().equals(user.getPassword())) {
			MHLogger.traceExit("AccountServiceImpl", "authenticateUser");
			return daouser;
		} else {			
			//Throws an exception otherwise
			MHLogger.traceExit("AccountServiceImpl", "authenticateUser");
			
			//Clear the password so controller doesn't pass it back with the encryption
			user.setPassword("");
			
			throw new UserNotAuthenticatedException("Incorrect password");
		}
	}
	
	@Override
	public List<User> listAllUsers(String userName) throws Exception {
		MHLogger.traceEntry("AccountServiceIml", "listAllUsers");
		ResultSet daoUsers = this.userdao.readListT();;
		List<User> users = new ArrayList<User>();
		try {
			while(daoUsers.next()) {
				if(!daoUsers.getString("user_name").equals(userName)) {
					users.add(new User(
						daoUsers.getInt("profile_id"),
						daoUsers.getString("full_name"), 
						daoUsers.getString("email"), 
						daoUsers.getString("user_name"), 
						null,
						daoUsers.getBoolean("admin")
						));
				}
			}
		MHLogger.traceExit("AccountServiceImpl", "listAllUsers");
		return users;
		}catch(Exception e) {
			throw new InternalServerException("There was a problem reading the list of users");
		} finally {
			daoUsers.close();
		}
	}

	@Override
	public User readUserById(int id) throws Exception {
		MHLogger.traceEntry("AccountServiceImpl", "authenticateUser");

		//Get the user's full account from the database
		MHLogger.infoLog("Getting the user's information from the dao layer");
		User daouser;
		//Get the next/first row of the return
		try {
			ResultSet daors = userdao.readTById(id);
			
			daors.next();
			//Generate the user model
			daouser = new User(
					daors.getInt("profile_id"),
					daors.getString("full_name"), 
					daors.getString("email"), 
					daors.getString("user_name"), 
					daors.getString("password"),
					daors.getBoolean("admin")
					);
			daors.close();
			
			return daouser;
		} catch(Exception e) {
			//Print error and let front end know what went wrong
			MHLogger.errorLog("AccountServiceImpl", "authenticateUser", e);
			throw new InternalServerException("There was an issue when retrieving the account.");
		}
	}
	
	@Override
	public void updateUserAccount(User user) throws Exception {
		this.userdao.updateT(user);
	}

	@Override
	public void deleteAccountById(int id) throws Exception {
		this.userdao.deleteTById(id);
	}
	
	/**
	 * <p>
	 * Generates a random string of randome size between 32 and 96 to be used
	 * with password encryption
	 * </p>
	 * 
	 * @return The random string
	 */
	private String generateSalt() {
		//Define the constant string
		String choices = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*";
		
		//Generate a random int for size of string
		int size = new Random().nextInt(64);
		MHLogger.debugLog("String size supposed to be " + size);
		//Add random characters from the constant to a string builder
		StringBuilder salt = new StringBuilder();
		for(int i = 0; i < size; i++) {
			salt.append(choices.charAt(new Random().nextInt(choices.length())));
		}
		
		//Pass the string
		return salt.toString();
	}
	
	/**
	 * <p>
	 * Hashes the password with a SHA-512 encryption for one way hashing.
	 * </p>
	 * 
	 * @param password The password to encrypt
	 * 
	 * @param salt The string to produce noise in the encryption
	 * 
	 * @return Fully encrypted password
	 */
	private String hashPassword(String password, String salt) throws Exception{
		//Create an instance of the security class
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        
        //Pass the salt to the security class
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        
        //Pass the password to the security class and get byte array
        byte[] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
        
        //Generate string from byte array
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++){
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        
        //Pass the new password back
        return sb.toString();
	}

}
