package com.mh.business;

import java.util.List;

/**
 * <p>
 * Interface for account business logic. Any and all logic associated with the 
 * account should take place in this interface and child classes.
 * </p>
 * 
 * @author Nathan Ford
 *
 * @param <T> The model that is responsible for a user's account
 * @param <P> The model that is responsible for password updates
 */
public interface AccountServiceInterface<T> {


	/**
	 * <p>
	 * Will take a user model and generate a random salt of random size for the user. It will 
	 * Then pass the password to be encrypted with a one way algorithm before being sent to the
	 * data layer along with the salt for storage in the database.
	 * </p>
	 * @param user the full information of the user account that is being created
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void createUserAccount(T user) throws Exception;
	
	/**
	 * <p>
	 * This method will take the user passed as a parameter and retrieve the full account
	 * from the dao layer based off the identifier to be specified by the child class. It will
	 * then encrypt the password of the user model that is logging in with the salt that was retrieved 
	 * from the database and compare it against the password from the database.
	 * </p>
	 * 
	 * @param user User model with an identifier field and a password filled out.
	 * 
	 * @return The full account of the user to be used by the presentation layer
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public T authenticateUser(T user) throws Exception;
	
	/**
	 * <p>
	 * Reads a user account by their id
	 * </p>
	 * @param user The user object
	 * @return The user object
	 * @throws Exception Custom exception of error
	 */
	public T readUserById(int id) throws Exception;
	
	/**
	 * <p>
	 * Takes a user account and updates it in the database, will not update any password or 
	 * salt fields. If you wish to update the password along with this method ensure that you 
	 * have the user fill out a PasswordChange model and pass it to {@link #updateUserPassword}
	 * </p>
	 * 
	 * @param user The user account with all but one identifier field filled out.
	 * 
	 * @return The new account information for the presentation layer to use
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void updateUserAccount(T user) throws Exception;
	
	/**
	 * <p>
	 * Returns a list of all the users with the application except for the user making the call
	 * </p>
	 * @return The complete list of all users excluding the user making the request
	 * 
	 * @throws Exception Pass through exception from the dao layer.
	 */
	public List<T> listAllUsers(String userName) throws Exception;
	
	/**
	 * <p>
	 * Calls the dao to delete the account based off it's primary key.
	 * </p>
	 * @param id The id of the account to be deleted
	 * 
	 * @throws Exception Custom exception to the error that occurred
	 */
	public void deleteAccountById(int id) throws Exception;
}
