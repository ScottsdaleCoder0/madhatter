package com.mh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mh.Product;
import com.mh.business.ProductServiceInterface;
import com.mh.utilities.MHLogger;
import com.mh.utilities.SessionUser;


// Set URI mapping to /product and set class to controller
@RequestMapping("/product")
@Controller
public class ProductController {
	
	ProductServiceInterface<Product> productService;
	public void setProductService(ProductServiceInterface<Product> productService) {
		this.productService = productService;
	}
	
	/**
	 * <p>
	 * Sends a product to the business layer to be created in the database
	 * </p>
	 * @param product The product being created
	 * @param result The result binding that contains form validation error
	 * @param model The map of the form model
	 * @return 
	 */
	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public ModelAndView createProduct(@Valid @ModelAttribute("product")Product product, BindingResult result, ModelMap model) {
		MHLogger.traceEntry("ProductController", "createProduct");
		//Validate the form 
		if (result.hasErrors()) {
			MHLogger.traceExit("ProductController", "createProduct");
			return new ModelAndView("product", "product", new Product());
		}
		
		MHLogger.debugLog(product.toString());
		
		// Add Product to database
		try {
			productService.createProduct(product);
			
			List<Product> products = this.productService.listProducts();
			
			Map<String, Object> map = SessionUser.getSessionMap();
			
			map.put("payload", products);
			
			MHLogger.traceExit("ProductController", "createProduct");
			return new ModelAndView("listProduct", map);
		} catch(Exception e) {
			MHLogger.errorLog("ProductController", "createProduct", e);
			MHLogger.traceExit("ProductController", "createProduct");
			
			return new ModelAndView("createProduct", "product", new Product());
		}
	}
	
	/**
	 * <p>
	 * Gets the view to list all products, works in conjunction with the {@link #getProductList()} to display
	 * all the products.
	 * </p>
	 * @return The list products view
	 */
	@RequestMapping(path = "/list", method = RequestMethod.GET)
	public ModelAndView listProducts() {
		MHLogger.traceEntry("ProductController", "listProducts");
		
		//Get the session map
		Map<String, Object> map = SessionUser.getSessionMap();
		
		//Return the view
		MHLogger.traceExit("ProductController", "listProducts");
		return new ModelAndView("listProduct", map);
	}
	
	/**
	 * <p>
	 * Gets the information of the product being purchased and sends it to the business layer.
	 * </p>
	 * @param product The product being purchased
	 * 
	 * @param result The results of the form
	 * 
	 * @param model The map of the model data in the form
	 * 
	 * @return The home page on success the same product page on failure
	 */
	@RequestMapping(path="/purchase", method=RequestMethod.POST)
	public ModelAndView purchaseProduct(@Valid @ModelAttribute("product")Product product, BindingResult result, ModelMap model) {
		MHLogger.traceEntry("ProductController", "purhcaseProduct");
		Map<String, Object> map = SessionUser.getSessionMap();
		try {
			this.productService.purchaseProduct(product);
			
			List<Product> products = this.productService.listProducts();
			
			map.put("payload", products);
			
			return new ModelAndView("home", map);
			
		} catch(Exception e) {
			MHLogger.errorLog("ProductController", "purchaseProduct", e);
			MHLogger.traceExit("ProductController", "purhcaseProduct");
			
			map.put("payload", product);
			return new ModelAndView("product", map);
		}
	}
	
	/**
	 * <p>
	 * Deletes a product from the database
	 * </p>
	 * @param id The id of the object being deleted
	 */
	@RequestMapping(path="/rest/delete/{id}", method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteProduct(@PathVariable("id") int id) {
		MHLogger.traceEntry("ProductController", "deleteProduct");
		try {
			this.productService.deleteProductById(id);
	
			MHLogger.traceExit("ProductController", "deleteProduct");
		}catch(Exception e) {
			MHLogger.errorLog("ProductController", "deleteProduct", e);
			MHLogger.traceExit("ProductController", "deleteProduct");
		}
		
	}
	
	/**
	 * <p>
	 * Gets the view with the product information to be updated
	 * </p>
	 * @param id The id of the product to update
	 * 
	 * @return The edit view on success The list products view on failure
	 */
	@RequestMapping(path="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editProduct(@PathVariable("id") int id) {
		MHLogger.traceEntry("ProductController", "deleteProduct");
		try {
			
			Product product = this.productService.getProductById(id);
			
			Map<String, Object> map = SessionUser.getSessionMap();
			
			map.put("payload", product);
			
			MHLogger.traceExit("ProductController", "deleteProduct");
			return new ModelAndView("editProduct", map);
		}catch(Exception e) {
			MHLogger.errorLog("ProductController", "deleteProduct", e);
			MHLogger.traceExit("ProductController", "deleteProduct");
			//Get the session map
			Map<String, Object> map = SessionUser.getSessionMap();
			
			//Return the view
			return new ModelAndView("listProduct", map);
		}
	}
	
	/**
	 * <p>
	 * Updates the product in the database by passing the updated model to the business layer
	 * </p>
	 * @param product The product model
	 * @param result The binded results
	 * @param model The map of the model and its attributes
	 * @return The list products page on success, the edit product page on failure
	 */
	@RequestMapping(path="/edit", method=RequestMethod.POST)
	public ModelAndView editProduct(@Valid @ModelAttribute("product")Product product, BindingResult result, ModelMap model) {
		MHLogger.traceEntry("ProductController", "editProduct");
		Map<String, Object> map = SessionUser.getSessionMap();
		map.put("payload", product);
		//Validate the form 
		if (result.hasErrors()) {
			MHLogger.traceExit("ProductController", "editProduct");
			return new ModelAndView("editProduct", map);
		}
		
		// Add Product to database
		try {
			productService.updateProduct(product);
			
			List<Product> products = this.productService.listProducts();			
			
			map.replace("payload", products);
			
			MHLogger.traceExit("ProductController", "editProduct");
			
			return new ModelAndView("listProduct", map);
		} catch(Exception e) {
			MHLogger.errorLog("ProductController", "editProduct", e);
			MHLogger.traceExit("ProductController", "editProduct");
			return new ModelAndView("editProduct", "product", new Product());
		}
	}
	
	/**
	 * <p>
	 * Gets a list of the products and binds it to the home page
	 * </p>
	 * @return The home page with products on success, empty page on failure
	 */
	@RequestMapping(path = "/home", method = RequestMethod.GET)
	public ModelAndView home() {
		MHLogger.traceEntry("ProductController", "home");
		Map<String, Object> map = SessionUser.getSessionMap();
		try {
			List<Product> products = this.productService.listProducts();
			
			map.put("payload", products);
			
			MHLogger.traceExit("ProductController", "home");
			return new ModelAndView("home", map);
		}catch(Exception e) {
			MHLogger.errorLog("ProductController", "home", e);
			MHLogger.traceExit("ProductController", "home");
			map.put("errorMessage", e.getMessage());
			map.put("payload", new ArrayList<Product>());
			return new ModelAndView("home", map); 
		}
		
	}

	/**
	 * <p>
	 * Gets the list of products in a json format
	 * </p>
	 * @return The json list of the products, an empty array on failure
	 */
	@RequestMapping(value="/rest/list", method=RequestMethod.GET)
	@ResponseBody
	public List<Product> getProductList(){
		MHLogger.traceEntry("ProductController", "getProductList");
		List<Product> products;
		try {
			products = this.productService.listProducts();
			MHLogger.traceExit("ProductController", "getProductList");
			return products;
		} catch (Exception e) {
			MHLogger.errorLog("ProductController", "getProductList", e);
			MHLogger.traceExit("ProductController", "getProductList");
			return new ArrayList<Product>();
		}
	}
	
	/**
	 * <p>
	 * Generates the view for the individual product
	 * </p>
	 * 
	 * @param id The database id of the prodduct
	 * 
	 * @return The View with the model binded to it
	 */
	@RequestMapping(value="/item/{id}", method=RequestMethod.GET)
	public ModelAndView showProductView(@PathVariable("id") int id) {
		MHLogger.traceEntry("ProductController", "showProductView");
		Map<String, Object> map = SessionUser.getSessionMap();
		try {
			Product temp = this.productService.getProductById(id);
			
			map.put("payload", temp);
			map.put("product", temp);
			MHLogger.traceExit("ProductController", "showProductView");
			return new ModelAndView("product", map);
			
		} catch(Exception e) {
			MHLogger.errorLog("ProductController", "showProductView", e);
			MHLogger.traceExit("ProductController", "showProductView");
			map.put("errorMessage", e.getMessage());
			map.put("payload", new ArrayList<Product>());
			return new ModelAndView("home", map);
		}
	}
	
	/**
	 * Gets the create product page 
	 * @return The view with a new product binded to it
	 */
	@RequestMapping(path = "/createProduct", method = RequestMethod.GET)
	public ModelAndView createProduct() {
		MHLogger.traceEntry("DefaultController", "createProduct");
		return new ModelAndView("createProduct", "product", new Product());
	}
}
