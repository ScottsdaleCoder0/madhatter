/*
 * This Controller Class is designed as the default traffic cop to navigate
 * and route the existing JSP files to URIS.  This Class was developed by
 * Nathan Ford, Jordan Nicols, and Adrian. This is our own work
 */
package com.mh.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mh.Registration;
import com.mh.User;
import com.mh.utilities.MHLogger;

@Controller
public class DefaultController {
	
	/**
	 * <p>
	 * Navigates you to the login view from the root url
	 * </p>
	 * @return The login view
	 */
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public ModelAndView goHome() {
		MHLogger.traceEntry("DefaultController", "goHome");
		return new ModelAndView("login", "user", new User());
	}
	
	/**
	 * <p>
	 * Navigates user directly to login page
	 * </p>
	 * @return The login view
	 */
	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public ModelAndView logUser() {
		MHLogger.traceEntry("DefaultController", "goHome");
		return new ModelAndView("login", "user", new User());
	}

	/**
	 * <p>
	 * Navigates the user directly to the register page
	 * </p>
	 * @return The registration view
	 */
	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public ModelAndView register() {
		MHLogger.traceEntry("DefaultController", "register");
		return new ModelAndView("register", "user", new Registration());
	}

}
