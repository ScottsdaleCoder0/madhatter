/*
 * This Controller class is designed to handle the request sent from login and registration.
 * This class will determine what action is happening, validate and bind the data and attempt to
 * store the data entered.  This class was developed by Nathan Ford, Jordan Nicols, and Adrian. 
 * This is our own work.
 */
package com.mh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.mh.Product;
import com.mh.Registration;
import com.mh.User;
import com.mh.business.AccountServiceInterface;
import com.mh.business.ProductServiceInterface;
import com.mh.utilities.MHLogger;
import com.mh.utilities.SessionUser;

//SET THE URI MAPPING TO /user AND SET CLASS TO CONTROLLER
@RequestMapping("/user")
@Controller
public class UserController {
	
	AccountServiceInterface<User> accountService;
	
	ProductServiceInterface<Product> productService;
	public void setProductService(ProductServiceInterface<Product> productService) {
		this.productService = productService;
	}
	
	public void setAccountService(AccountServiceInterface<User> accountService) {
		this.accountService = accountService;
	}
	
	/**
	 * <p>
	 * Gateway to register a user using a Registration model. This method will confirm the two passwords match and 
	 * make the appropriate conversion.
	 * </p>
	 * @param user The Registration model of the user
	 * 
	 * @param result The results of binding the model and Interface
	 *  
	 * @param model The Map of the model attributes
	 * 
	 * @return The appropriate view
	 */
	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public ModelAndView registerUser(@Valid @ModelAttribute("user")Registration user, BindingResult result, ModelMap model) {
		MHLogger.traceEntry("UserController", "registerUser");
		//Validate the password fields
		if(!user.getPassword().equals(user.getConfPassword())) {
			result.addError(new FieldError("user", "password", "Passwords must match"));
			result.addError(new FieldError("user", "confPassword", "Passwords must match"));
		}
		//Return the old view with errors
		if (result.hasErrors()) {
			MHLogger.traceExit("UserController", "registerUser");
			return new ModelAndView("register", "user", user);
		}
		
		//Bind the data from the view to the model
		model.addAttribute("fullName", user.getFullName());
		model.addAttribute("email", user.getEmail());
		model.addAttribute("address", user.getUsername());
		model.addAttribute("password", user.getPassword());
		
		//Add user to the application
		try {
			MHLogger.debugLog(user.getFullName());
			//Make the account
			accountService.createUserAccount(user.toUser());
			
			//Return user to login so they can authenticate themselves with new information
			MHLogger.traceExit("UserController", "registerUser");
			return new ModelAndView("login", "user", new User());			
		}catch(Exception e) {
			//Log all exceptions
			MHLogger.errorLog("UserController", "registerUser", e);
			MHLogger.traceExit("UserController", "registerUser");
			//Return user to the registration view
			return new ModelAndView("register", "user", new User());
			
		}
	}
	
	/**
	 * <p>
	 * Gateway to log the user in. Binds the login information to a user model and passes it for authentication to the business layer.
	 * </p>
	 * @param user The user model
	 * @param result The binded results
	 * @param model The Map of the model that was used on the form
	 * @return The appropriate view for the user
	 */
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public ModelAndView loginUser(@Valid @ModelAttribute("user")User user, BindingResult result, ModelMap model) {
		MHLogger.traceEntry("UserController", "loginUser");
		//Validate the form
		if(result.hasErrors()) {
			MHLogger.traceExit("UserController", "loginUser");
			return new ModelAndView("login", "user", user);
		}
		//Bind the data from the form
		model.addAttribute("email", user.getEmail());
		model.addAttribute("password", user.getPassword());
		
		try {
			//Authenticate the user from the database
			user = accountService.authenticateUser(user);
			
			//Create a session for the user
			RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
		    ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
		    HttpServletRequest request = attributes.getRequest();
		    HttpSession httpSession = request.getSession(true);
		    
		    //NOTE: Ensure any additions here are reflected in logout.
		    httpSession.setAttribute("isAdmin", user.getIsAdmin());
		    httpSession.setAttribute("username", user.getUsername());
			
		    Map<String, Object> map = SessionUser.getSessionMap();
	    	
		    //Get a list of products for the home page
		    List<Product> products = this.productService.listProducts();
			
		    //Put the products in the returned information
			map.put("payload", products);
			MHLogger.traceExit("UserController", "loginUser");
			
			//Return the view
			return new ModelAndView("home", map);
			
		}catch(Exception e) {
			//Log any errors that occured
			MHLogger.errorLog("UserController", "loginUser", e);
			MHLogger.traceExit("UserController", "loginUser");
			
			//Return the user to the login page for failed authentication
			return new ModelAndView("login", "user", user);
		}
	}
	
	/**
	 * <p>
	 * Gateway to get the view to list the users. 
	 * </p>
	 * @return The list users view
	 */
	@RequestMapping(path="/list", method=RequestMethod.GET)
	public ModelAndView listUsers() {
		MHLogger.traceEntry("UserController", "listUsers");
		Map<String, Object> map = SessionUser.getSessionMap();
		
		MHLogger.traceExit("UserController", "listUsers");
		return new ModelAndView("listUsers", map);
	}
	
	/**
	 * <p>
	 * Gateway to get a json list of users using the application
	 * </p>
	 * 
	 * @return List of all users using the application
	 */
	@RequestMapping(path="/rest/list", method=RequestMethod.GET)
	@ResponseBody
	public List<User> listAllUsers(){
		try {
			MHLogger.traceEntry("UserController", "listAllUsers");
			
			Map<String, Object> map = SessionUser.getSessionMap();
			
			String username = (String) map.get("username");
			
			List<User> users = this.accountService.listAllUsers(username);
			MHLogger.traceExit("UserController", "listAllUsers");
			return users;
		}catch(Exception e) {
				MHLogger.errorLog("UserController", "listAllUsers", e);
				MHLogger.traceExit("UserController", "listAllUsers");
				return new ArrayList<User>();
		}
		
	}
	
	/**
	 * <p>
	 * Communicates with the business layer to update a user's admin status
	 * </p>
	 * @param id The id of the user
	 */
	@RequestMapping(path="/rest/admin/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public void toggleAdmin(@PathVariable("id") int id) {
		try {
			MHLogger.traceEntry("UserController", "toggleAdmin");
			
			User user = this.accountService.readUserById(id);
			
			user.setIsAdmin(!user.getIsAdmin());
			
			this.accountService.updateUserAccount(user);
			
			MHLogger.traceExit("UserController", "toggleAdmin");
		}catch(Exception e) {
			MHLogger.errorLog("UserController", "toggleAdmin", e);
			MHLogger.traceExit("UserController", "toggleAdmin");
		}
	}
	
	/**
	 * <p>
	 * Deletes a user from the database
	 * </p>
	 * @param id The id of the user to be deleted
	 */
	@RequestMapping(path="/rest/delete/{id}", method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteUser(@PathVariable("id") int id) {
		try {
			MHLogger.traceEntry("UserController", "toggleAdmin");
			
			this.accountService.deleteAccountById(id);
			
			MHLogger.traceExit("UserController", "toggleAdmin");
		}catch(Exception e) {
			MHLogger.errorLog("UserController", "toggleAdmin", e);
			MHLogger.traceExit("UserController", "toggleAdmin");
		}
	}
	
	/**
	 * <p>
	 * Clears the session of all the user's variables and returns them to the login view
	 * </p>
	 * @return A redirection to the login url
	 */
	@RequestMapping(path = "/logout", method = RequestMethod.GET)
	public RedirectView logUserOut() {
		MHLogger.traceEntry("UserController", "logUserOut");
		//Get the session
		RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
	    ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
	    HttpServletRequest request = attributes.getRequest();
	    HttpSession httpSession = request.getSession(false);
	    
	    //NOTE: Ensure these are removing everything put in by login.
	    //Remove the attributes from the session
	    httpSession.removeAttribute("isAdmin");
	    httpSession.removeAttribute("username");
		
	    //Redirect the user back to home
	    MHLogger.traceExit("UserController", "logUserOut");
		return new RedirectView("/madhatter");
	}
}
