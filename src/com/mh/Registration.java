/*
 * This Java class serves as the User model for the applications, storing important user data in this
 * model upon registration and login.  This model was build as a part of the MadHatters Project in 
 * CST-341 and was built by Nathan Ford, Jordan Nicols, and Adrian. This is our own work.
 */
package com.mh;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;

public class Registration {
	@Size(max = 30, message = "Invalid number of characters")
	@NotNull(message = "Required field")
	private String username;

	@Size(max = 100, message = "Invalid number of characters")
	@NotNull(message="Required field")
	private String fullName;
	
	@NotNull(message = "Required field")
	@Size(min = 5, max = 50, message = "Invalid number of characters")
	@Email(message="Not a valid email address")
	private String email;
	
	@NotNull(message = "Required field")
	@Size(min = 6, message = "Password must be a minimum of 6 characters")
	private String password;
	
	@NotNull(message = "Required field")
	@Size(min = 6, message = "Password must be a minimum of 6 characters")
	private String confPassword;
	
	// CREATE DEFAULT CONSTRUCTOR FOR USER MODEL
	public Registration() {
		this.fullName = "";
		this.email = "";
		this.username = "";
		this.password = "";
		this.confPassword = "";
	}	
	
	// CREATE NON-DEFAULT CONSTRUCTOR
	public Registration(String fullname, String email, String username, String password, String confPassword) {
		this.fullName = fullname;
		this.email = email;
		this.username = username;
		this.password = password;
		this.confPassword = confPassword;
	}
	
	// GETTERS AND SETTERS
	public String getFullName() {
		return fullName;
	}
	
	public String getConfPassword() {
		return confPassword;
	}

	public void setConfPassword(String confPassword) {
		this.confPassword = confPassword;
	}

	public void setFullName(String fullname) {
		this.fullName = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public User toUser() {
		return new User(0, this.fullName, this.email, this.username, this.password, false);
	}
}
