package com.mh;

public class Product {
	private int id;
	private String type;
	private String color;
	private String size;
	private byte[] image;
	private int quantity;
	private double price;
	
	public Product(int id, String type, String color, String size, byte[] image, int quantity, double price) {
		super();
		this.id = id;
		this.type = type;
		this.color = color;
		this.size = size;
		this.image = image;
		this.quantity = quantity;
		this.price = price;
	}
	
	public Product() {
		this.id = -1;
		this.type = "";
		this.color = "";
		this.size = "";
		this.image = new byte[0];
		this.quantity = 0;
		this.price = 0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// Getters and Setters
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Type: " + this.type + "\n"
				+ "Color: " + this.color + "\n"
				+ "Size: "+ this.size +"\n"
				+ "Quantity: "+ this.quantity +"\n"
				+ "Price: "+ this.price;
	}
	
}
