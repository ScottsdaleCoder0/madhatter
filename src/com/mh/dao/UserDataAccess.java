package com.mh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import com.mh.User;
import com.mh.exception.RecordNotCreatedException;
import com.mh.exception.RecordNotDeletedException;
import com.mh.exception.RecordNotFoundException;
import com.mh.exception.RecordNotUpdatedException;
import com.mh.utilities.DBConnection;
import com.mh.utilities.MHLogger;


public class UserDataAccess implements DataAccessInterface<User, String> {

	private Connection conn = null;
	
	@Override
	public void createT(User t, String salt) throws RecordNotCreatedException {
		try {
			MHLogger.traceEntry("UserDataAccess", "createT");
			this.conn = DBConnection.getConnection();
			//Create the sql
			MHLogger.infoLog("Generating the query");
			String sql = "INSERT INTO profile "
					+ "(user_name, full_name, email, salt, admin, password) "
					+ "VALUES ("
					+ "?, " //1: username
					+ "?, " //2: fullname
					+ "?, " //3: email
					+ "?, " //4: salt
					+ "?, " //5: admin
					+ "?)"; //6: password
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, t.getUsername());
			ps.setString(2, t.getFullName());
			ps.setString(3, t.getEmail());
			ps.setString(4, salt);
			ps.setBoolean(5, t.getIsAdmin());
			ps.setString(6, t.getPassword());
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ps.execute();
			
			MHLogger.traceExit("UserDataAccess", "createT");
			
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("UserDataAccess", "createT", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("UserDataAccess", "createT");
			throw new RecordNotCreatedException("Internal server error when creating account for " + t.getUsername());
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("UserDataAccess", "createT", e);
				}
			}
		}
	}
	
	@Override
	public void createT(User t) throws RecordNotCreatedException {
		MHLogger.warnLog("Some code is using an unimplemented method, please identify and correct this issue.");
		throw new RecordNotCreatedException("Not implemented");
	}

	@Override
	public ResultSet readTById(int id) throws RecordNotFoundException {
		try {
			MHLogger.traceEntry("UserDataAccess", "readTById");
			this.conn = DBConnection.getConnection();
			//Create the sql
			MHLogger.infoLog("Generating the query");
			
			String sql = "Select profile_id, user_name, full_name, email, admin, password "
					+ "from profile where profile_id = ?";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ResultSet rs = ps.executeQuery();
			
			MHLogger.traceExit("UserDataAccess", "readTById");
			return rs;
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("UserDataAccess", "readTById", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("UserDataAccess", "readTById");
			throw new RecordNotFoundException("Internal server error when looking up account for primary key field");
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("UserDataAccess", "readTById", e);
				}
			}
		}
	}

	@Override
	public ResultSet readTByUniqueField(String unique) throws RecordNotFoundException {
		try {
			MHLogger.traceEntry("UserDataAccess", "readTByUniqueField");
			this.conn = DBConnection.getConnection();
			
			//Create the sql
			MHLogger.infoLog("Generating the query");
			
			String sql = "Select profile_id, user_name, full_name, email, salt, admin, password "
					+ "from profile where email = ?";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, unique);
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ResultSet rs = ps.executeQuery();
			
			MHLogger.traceExit("UserDataAccess", "readTByUniqueField");
			return rs;
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("UserDataAccess", "readTByUniqueField", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("UserDataAccess", "readTByUniqueField");
			throw new RecordNotFoundException("Internal server error when looking up account for " + unique);
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("UserDataAccess", "readTByUniqueField", e);
				}
			}
		}
	}

	@Override
	public void updateT(User t) throws RecordNotUpdatedException {
		try {
			MHLogger.traceEntry("UserDataAccess", "updateT");
			this.conn = DBConnection.getConnection();
			
			//Create the sql
			MHLogger.infoLog("Generating the query");
			
			String sql = "UPDATE profile SET "
					+ "user_name = ?,"
					+ "full_name = ?,"
					+ "email = ?,"
					+ "admin = ?"
					+ "where profile_id = ?";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, t.getFullName());
			ps.setString(2, t.getFullName());
			ps.setString(3, t.getEmail());
			ps.setBoolean(4, t.getIsAdmin());
			ps.setInt(5, t.getId());
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ps.execute();
			
			MHLogger.traceExit("UserDataAccess", "updateT");
			
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("UserDataAccess", "updateT", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("UserDataAccess", "updateT");
			throw new RecordNotUpdatedException("Internal server error when updating account for " + t.getUsername());
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("UserDataAccess", "updateT", e);
				}
			}
		}
		
	}

	@Override
	public void deleteTById(int id) throws RecordNotDeletedException {
		try {
			MHLogger.traceEntry("UserDataAccess", "deleteTById");
			this.conn = DBConnection.getConnection();
			
			//Create the sql
			MHLogger.infoLog("Generating the query");
			
			String sql = "DELETE FROM profile WHERE profile_id = ?";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ps.execute();
			
			MHLogger.traceExit("UserDataAccess", "deleteTById");
			
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("UserDataAccess", "deleteTById", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("UserDataAccess", "deleteTById");
			throw new RecordNotDeletedException("Internal server error when deleting account using primary key");
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("UserDataAccess", "deleteTById", e);
				}
			}
		}
		
	}

	@Override
	public ResultSet readListT() throws RecordNotFoundException {
		try {
			MHLogger.traceEntry("UserDataAccess", "readListT");
			this.conn = DBConnection.getConnection();
			
			//Create the sql
			MHLogger.infoLog("Generating the query");
			
			String sql = "Select profile_id, user_name, full_name, email, admin from profile";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ResultSet rs = ps.executeQuery();
			
			MHLogger.traceExit("UserDataAccess", "readListT");
			return rs;
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("UserDataAccess", "readListT", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("UserDataAccess", "readTById");
			throw new RecordNotFoundException("Internal server error when looking up account for primary key field");
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("UserDataAccess", "readListT", e);
				}
			}
		}
	}
}
