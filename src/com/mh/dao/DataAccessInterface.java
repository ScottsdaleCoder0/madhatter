package com.mh.dao;


import java.sql.ResultSet;

import com.mh.exception.RecordNotCreatedException;
import com.mh.exception.RecordNotDeletedException;
import com.mh.exception.RecordNotFoundException;
import com.mh.exception.RecordNotUpdatedException;

/**
 * <p>
 * Interface to ensure that all dao objects have the needed crud operations
 * </p>
 * 
 * @author Nathan Ford
 *
 */
public interface DataAccessInterface<T, U> {

	/**
	 * <p>
	 * Creates a record in the database with additional information that you do not want attached to the model.
	 * </p>
	 * 
	 * @param t The object that you are creating in the database
	 * 
	 * @param add Any additional information you wish to keep out of the model class
	 * 
	 * @throws RecordNotCreatedException Indicates that something went wrong with the interaction. Will contain 
	 * a user friendly message.
	 */
	public void createT(T t, U add) throws RecordNotCreatedException;
	
	
	/**
	 * <p>
	 * Creates a record in the database without needing any additional information attatched to it.
	 * </p>
	 * 
	 * @param t The object that is being created
	 * 
	 * @throws RecordNotCreatedException Indicates that something went wrong with the interactions. Will contain
	 * a user friendly message.
	 */
	public void createT(T t) throws RecordNotCreatedException;
	
	/**
	 * <p>
	 * Reads an object from the database from the id field
	 * </p>
	 * 
	 * 
	 * @param id
	 * @throws RecordNotFoundException
	 */
	public ResultSet readTById(int id) throws RecordNotFoundException;
	
	/**
	 * <p>
	 * Reads a record from the database by retrieving it from a unique field. See child implementation 
	 * for further information on what type of data needs to be passed.
	 * </p>
	 * 
	 * @param unique The unique parameter that will be used to retrieve the object from the database.
	 * 
	 * @return The result set containing the information retrieved from the database
	 * 
	 * @throws RecordNotFoundException Indicates something went wrong along the way. Will contain a user friendly
	 * message of what went wrong. 
	 */
	public ResultSet readTByUniqueField(U unique) throws RecordNotFoundException;
	
	/**
	 * <p>
	 * Reads a list of objects
	 * </p>
	 * @return A ResultSet containing a list of T
	 * 
	 * @throws RecordNotFoundException Indicates something went wrong with the transaction with the database
	 */
	public ResultSet readListT() throws RecordNotFoundException;
	
	/**
	 * <p>
	 * Updates a record in the database
	 * </p>
	 * 
	 * @param t The record that will be updated. Ensure there is an attached id.
	 * 
	 * @throws RecordNotUpdatedException Indicates something went wrong, will contain user friendly message.
	 */
	public void updateT(T t) throws RecordNotUpdatedException;
	
	/**
	 * <p>
	 * Deletes a record from the database using the primary key of the table
	 * </p>
	 * 
	 * @param id The primary key id for the record in the database
	 * 
	 * @throws RecordNotDeletedException Indicates record was not successfully deleted and will have a user
	 * friendly message attached
	 */
	public void deleteTById(int id) throws RecordNotDeletedException;
}
