package com.mh.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mh.Product;
import com.mh.exception.RecordNotCreatedException;
import com.mh.exception.RecordNotDeletedException;
import com.mh.exception.RecordNotFoundException;
import com.mh.exception.RecordNotUpdatedException;
import com.mh.utilities.DBConnection;
import com.mh.utilities.MHLogger;

public class ProductDataAccess implements DataAccessInterface<Product, String> {

	private Connection conn = null;
	@Override
	public void createT(Product t) throws RecordNotCreatedException {
		try {
			MHLogger.traceEntry("ProductDataAccess", "createT");
			this.conn = DBConnection.getConnection();
			
			// Create sql
			MHLogger.infoLog("Generating the query");
			String sql = "INSERT INTO hats "
					+ "(hat_type, color, hat_size, image, quantity, price) "
					+ "VALUES ("
					+ "?, " // 1: hat type
					+ "?, " // 2: color
					+ "?, " // 3: hat size
					+ "?, " // 4: image
					+ "?, " // 5: quantity
					+ "?)"; // 6: price
			
			// Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, t.getType());
			ps.setString(2, t.getColor());
			ps.setString(3, t.getSize());
			ps.setBytes(4, t.getImage());
			ps.setInt(5, t.getQuantity());
			ps.setDouble(6, t.getPrice());
			
			// Make the query
			MHLogger.infoLog("Making the query to the database");
			ps.execute();
			
			MHLogger.traceExit("ProductDataAccess", "createT");
		} catch(Exception e) {
			// Log error
			MHLogger.errorLog("ProductDataAccess", "createT", e);
			
			// Throw a user friendly exeption and trace the return
			MHLogger.traceExit("ProductDataAccess", "createT");
			throw new RecordNotCreatedException("Internal server error when creating account for " + t.getType());
		} finally {
			if (this.conn != null) {
				try {
					this.conn.close();
				} catch(Exception e) {
					MHLogger.errorLog("ProductDataAccess", "createT", e);
				}
			}
		}
	}
	
	@Override
	public void createT(Product t, String add) throws RecordNotCreatedException {
		MHLogger.warnLog("Some code is using an unimplemented method, please identify and correct this issue.");
		throw new RecordNotCreatedException("Not implemented");
	}

	@Override
	public ResultSet readTById(int id) throws RecordNotFoundException {
		try {
			MHLogger.traceEntry("ProductDataAccess", "readTById");
			this.conn = DBConnection.getConnection();
			
			//Create the sql
			MHLogger.infoLog("Generating the query");
			
			String sql = "Select hats_id, hat_type, color, hat_size, image, quantity, price from hats where hats_id = ?";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ResultSet rs = ps.executeQuery();
			
			MHLogger.traceExit("UserDataAccess", "readTByUniqueField");
			return rs;
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("ProductDataAccess", "readTById", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("ProductDataAccess", "readTById");
			throw new RecordNotFoundException("Internal server error when looking up product.");
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("ProductDataAccess", "readTById", e);
				}
			}
		}
	}

	@Override
	public ResultSet readTByUniqueField(String unique) throws RecordNotFoundException {
		MHLogger.warnLog("Some code is using an unimplemented method, please identify and correct this issue.");
		throw new RecordNotFoundException("Not implemented");
	}

	@Override
	public void updateT(Product t) throws RecordNotUpdatedException {
		try {
			MHLogger.traceEntry("ProductDataAccess", "updateT");
			this.conn = DBConnection.getConnection();
			
			// Create sql
			MHLogger.infoLog("Generating the query");
			String sql = "UPDATE hats SET "
					+ "hat_type = ?,"
					+ "color = ?,"
					+ "hat_size = ?,"
					+ "image = ?,"
					+ "quantity = ?,"
					+ "price = ?"
					+ "where hats_id = ?"; // 6: price
			MHLogger.debugLog("Hat quantity = " + t.getQuantity());
			// Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, t.getType());
			ps.setString(2, t.getColor());
			ps.setString(3, t.getSize());
			ps.setBytes(4, t.getImage());
			ps.setInt(5, t.getQuantity());
			ps.setDouble(6, t.getPrice());
			ps.setInt(7, t.getId());
			
			
			
			// Make the query
			MHLogger.infoLog("Making the query to the database");
			ps.execute();
			
			MHLogger.traceExit("ProductDataAccess", "updateT");
		} catch(Exception e) {
			// Log error
			MHLogger.errorLog("ProductDataAccess", "updateT", e);
			
			// Throw a user friendly exeption and trace the return
			MHLogger.traceExit("ProductDataAccess", "updateT");
			throw new RecordNotUpdatedException("Internal server error when updating product " + t.getType());
		} finally {
			if (this.conn != null) {
				try {
					this.conn.close();
				} catch(Exception e) {
					MHLogger.errorLog("ProductDataAccess", "updateT", e);
				}
			}
		}
		
	}

	@Override
	public void deleteTById(int id) throws RecordNotDeletedException {
		try {
			MHLogger.traceEntry("ProductDataAccess", "deleteTById");
			this.conn = DBConnection.getConnection();
			
			// Create sql
			MHLogger.infoLog("Generating the query");
			String sql = "DELETE FROM hats WHERE hats_id = ?";
			
			// Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);

			
			// Make the query
			MHLogger.infoLog("Making the query to the database");
			ps.execute();
			
			MHLogger.traceExit("ProductDataAccess", "deleteTById");
		} catch(Exception e) {
			// Log error
			MHLogger.errorLog("ProductDataAccess", "deleteTById", e);
			
			// Throw a user friendly exeption and trace the return
			MHLogger.traceExit("ProductDataAccess", "deleteTById");
			throw new RecordNotDeletedException("Internal server error when deleting product.");
		} finally {
			if (this.conn != null) {
				try {
					this.conn.close();
				} catch(Exception e) {
					MHLogger.errorLog("ProductDataAccess", "deleteTById", e);
				}
			}
		}
		
	}

	@Override
	public ResultSet readListT() throws RecordNotFoundException {
		try {
			MHLogger.traceEntry("ProductDataAccess", "readListT");
			this.conn = DBConnection.getConnection();
			
			String sql = "Select hats_id, hat_type, color, hat_size, image, quantity, price from hats where hats_id > 0";
			
			//Prepare the statement
			PreparedStatement ps = conn.prepareStatement(sql);
			
			//Make the query
			MHLogger.infoLog("Making the query to the database");
			ResultSet rs = ps.executeQuery();
			
			MHLogger.traceExit("ProductDataAccess", "readListT");
			return rs;
		} catch(Exception e) {
			//Log the error
			MHLogger.errorLog("ProductDataAccess", "readListT", e);

			//Throw a user friendly exception and trace the return
			MHLogger.traceExit("ProductDataAccess", "readListT");
			throw new RecordNotFoundException("Internal server error when retrieving list of products");
		}finally {
			if(this.conn != null) {
				try {
					this.conn.close();
				}catch(Exception e) {
					MHLogger.errorLog("ProductDataAccess", "readListT", e);
				}
			}
		}
	}
}
