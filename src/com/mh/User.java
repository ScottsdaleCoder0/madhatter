/*
 * This Java class serves as the User model for the applications, storing important user data in this
 * model upon registration and login.  This model was build as a part of the MadHatters Project in 
 * CST-341 and was built by Nathan Ford, Jordan Nicols, and Adrian. This is our own work.
 */
package com.mh;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {
	
	private int id;
	
	// DECLARE USER VARIABLES AND DATA VALIDATION ANNOTAIONS
	@Size(max = 100, message = "Invalid number of characters")
	private String fullName;
	
	@NotNull(message = "The Email Field is Required")
	@Size(min = 5, max = 50, message = "Invalid number of characters")
	private String email;
	
	@Size(max = 30, message = "Invalid number of characters")
	private String username;
	
	@NotNull(message = "The Password Field is Required")
	@Size(min = 6, message = "Password must be a minimum of 6 characters")
	private String password;
	
	private Boolean isAdmin;
	
	// CREATE DEFAULT CONSTRUCTOR FOR USER MODEL
	public User() {
		this.id = -1;
		this.fullName = "";
		this.email = "";
		this.username = "";
		this.password = "";
		this.isAdmin = false;
	}	
	
	// CREATE NON-DEFAULT CONSTRUCTOR
	public User(int id, String fullname, String email, String username, String password, Boolean isAdmin) {
		this.id = id;
		this.fullName = fullname;
		this.email = email;
		this.username = username;
		this.password = password;
		this.isAdmin = isAdmin;
	}
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	// GETTERS AND SETTERS
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullname) {
		this.fullName = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
