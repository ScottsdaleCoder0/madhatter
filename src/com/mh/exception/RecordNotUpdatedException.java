package com.mh.exception;

/**
 * <p>
 * Exception to indicate that there was a failure updating a record in the database.
 * </p>
 * @author Nathan Ford
 *
 */
public class RecordNotUpdatedException extends Exception {

	private static final long serialVersionUID = 5204374964196784509L;

	public RecordNotUpdatedException(String message) {
		super(message);
	}
}
