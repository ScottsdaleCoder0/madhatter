package com.mh.exception;

/**
 * <p>
 * Exception used to indicate that there was an issue while looking for records in the database
 * </p>
 * @author Nathan Ford
 *
 */
public class RecordNotFoundException extends Exception{

	private static final long serialVersionUID = -1113461589514842161L;

	public RecordNotFoundException(String message) {
		super(message);
	}
}
