package com.mh.exception;

/**
 * <p>
 * An exception to indicate that an issue occured while deleting a record in the database
 * </p>
 * @author Nathan Ford
 *
 */
public class RecordNotDeletedException extends Exception {

	private static final long serialVersionUID = 6947721998156411319L;

	public RecordNotDeletedException(String message) {
		super(message);
	}
}
