package com.mh.exception;

/**
 * <p>
 * An exception to indicate that a record was not created in the database
 * </p>
 * @author Nathan Ford
 *
 */
public class RecordNotCreatedException extends Exception {

	private static final long serialVersionUID = -2449419858936444797L;
	
	public RecordNotCreatedException(String message) {
		super(message);
	}

}
