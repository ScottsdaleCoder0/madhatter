package com.mh.exception;

/**
 * <p>
 * Exception to indicate that a user was not authenticated. 
 * This can indicate that passwords did not match.
 * </p>
 * @author Nathan Ford
 *
 */
public class UserNotAuthenticatedException extends Exception {

	private static final long serialVersionUID = -8072873166382725563L;

	public UserNotAuthenticatedException(String message) {
		super(message);
	}
}
