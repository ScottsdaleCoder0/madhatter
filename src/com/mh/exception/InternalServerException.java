package com.mh.exception;

/**
 * <p>
 * A broad exception to handle internal logic issues
 * </p>
 * @author Nathan Ford
 */
public class InternalServerException extends Exception {

	private static final long serialVersionUID = 454974279472726359L;

	public InternalServerException(String message) {
		super(message);
	}
}
